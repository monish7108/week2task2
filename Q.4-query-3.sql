/* 3. List the names of the customers, who have sent more than one shipment.  */

select CUST_NAME from CUSTOMER where CUST_ID in (select a.CUST_ID from  SHIPMENT a group by a.CUST_ID having count(a.CUST_ID)>1);
