/*2. List cities that have received shipments from every customer. ( use NOT EXISTS)
*	THERE are no such cities who got shipments from every customer.
*/

select CITY_NAME from CITY where not exists(
select DESTINATION from SHIPMENT 
where SHIPMENT.DESTINATION=CITY.CITY_NAME
group by DESTINATION 
having count(DISTINCT CUST_ID)!=(select count(*) from CUSTOMER));