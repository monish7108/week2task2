/* List drivers who have delivered shipments to every city  */

select DRIVER_NAME from TRUCK where TRUCK_NO in 
(select TRUCK_NO from TRUCK where (select count(distinct DESTINATION ) from SHIPMENT where
 SHIPMENT.TRUCK_NO = TRUCK.TRUCK_NO) = (select count(*) from CITY));


