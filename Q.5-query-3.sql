/* 3. List drivers who have delivered shipments to every city. */


select * from TRUCK where  not exists 
(select b.TRUCK_NO from SHIPMENT b where TRUCK.TRUCK_NO=b.TRUCK_NO group by b.TRUCK_NO
having count(DISTINCT b.DESTINATION)!=(select count(*) from CITY));
