/*4. List the names of the truck drivers, whose total weight of delivered shipment is more than 1000.*/

select a.DRIVER_NAME , sum(b.WEiGHT) as weight from TRUCK a, SHIPMENT b 
where a.TRUCK_NO=b.TRUCK_NO  
GROUP BY a.TRUCK_NO 
having weight>1000;