/* 8. List the names of the customers and total weight of shipments, 
if the total weight of shipment is the maximum among the total shipments of each customer. */


select a.CUST_ID,b.CUST_NAME, sum(WEIGHT) as sum from SHIPMENT a,CUSTOMER b
where b.CUST_ID=a.CUST_ID
 group by b.CUST_ID order by sum desc limit 1;
