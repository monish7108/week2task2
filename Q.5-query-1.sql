/*  1. List customers who had shipments delivered by every truck.  ( use NOT EXISTS) */

select * from CUSTOMER c where not exists
(select CUSTOMER.CUST_ID from CUSTOMER left join SHIPMENT 
on CUSTOMER.CUST_ID=SHIPMENT.CUST_ID
where c.CUST_ID=CUSTOMER.CUST_ID
group by CUSTOMER.CUST_ID
having count(DISTINCT TRUCK_NO)!=(select count(*) from TRUCK));


