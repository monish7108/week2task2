/*  7. List the names of drivers, who have delivered shipments 
weighing over 100 pounds taken cumulatively.   */

select DRIVER_NAME from TRUCK where TRUCK_NO in
 (select DISTINCT TRUCK_NO from SHIPMENT group by TRUCK_NO having  sum(WEIGHT)>100) ;