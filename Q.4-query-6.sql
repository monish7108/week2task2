/* 6. Display the names and the population of the city 
if the population is more than 100000000 and 
if more than one shipment has gone to the city. */

select CITY_NAME , POPULATION from CITY 
where POPULATION>100000000 and CITY_NAME in
 (select DESTINATION from SHIPMENT group by DESTINATION 
	having count(DESTINATION)>1 );