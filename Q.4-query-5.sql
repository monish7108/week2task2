/*   5. Display the name and annual revenue of the customer, who has not sent any shipment.  */


SELECT CUST_NAME , ANNUAL_REVENUE
FROM CUSTOMER 
WHERE NOT EXISTS 
    (SELECT CUST_ID 
     FROM SHIPMENT
     WHERE CUSTOMER.CUST_ID = SHIPMENT.CUST_ID);
